package com.inventory.service;
import com.inventory.model.User;

public interface UserService {
	public User findUserByEmail(String email);
	public User getCurrentUser();
	public void saveUser(User user);
	public String getCurrentUserName();
	public User findUserByUsername(String username);
}
package com.inventory.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public interface ICommonService<T> {
	 public T store(T entity);   
	 public List<T> getAll();  
	 public T getById(UUID id);  
	 public T update(T entity);  
	 public UUID delete(Map<String, String[]> requestMap);  
}

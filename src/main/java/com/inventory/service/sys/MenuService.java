package com.inventory.service.sys;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inventory.model.sys.Menu;
import com.inventory.repository.IMenuRepository;

@Service("menuService")
public class MenuService implements IMenuService {
	@Autowired
	private IMenuRepository menuRepository;

	@Override
	public Menu store(Menu entity) {
		UUID id = menuRepository.insertDoc(entity);
		return (id != null) ? entity : null;
	}

	@Override
	public List<Menu> getAll() {
		
		return menuRepository.getAllDoc();
	}

	@Override
	public Menu getById(UUID id) {
		// TODO Auto-generated method stub
		return menuRepository.getDocById(id);
	}

	@Override
	public Menu update(Menu entity) {
		UUID id = menuRepository.updateDoc(entity);
		return (id != null) ? entity : null;
	}

	@Override
	public UUID delete(Map<String, String[]> requestMap) {
		// TODO Auto-generated method stub
		return null;
	}

}

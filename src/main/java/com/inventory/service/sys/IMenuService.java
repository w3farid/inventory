package com.inventory.service.sys;

import com.inventory.model.sys.Menu;
import com.inventory.service.ICommonService;

public interface IMenuService extends ICommonService<Menu> {

}

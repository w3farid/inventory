package com.inventory.controller;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;

public interface ICommonController<T> {
	public ModelAndView index();
	public ModelAndView show(UUID id);
	public T get(UUID id);
	public ModelAndView create();
	public T store(HttpServletRequest request) ;
	public ModelAndView edit(UUID id);
	public T update(HttpServletRequest request);
	public T destroy(HttpServletRequest request);
	public ModelAndView showSearch(HttpServletRequest request);
	public String search(HttpServletRequest request);
	

}

package com.inventory.controller.sys;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.inventory.configuration.bean.RESTResponse;
import com.inventory.model.sys.Menu;
import com.inventory.service.sys.IMenuService;

@RestController
@RequestMapping("/menu/v1")
public class MenuController {
	private static final String VIEWS_MENU_CREATE_OR_EDIT_FORM = "sys/menu/createOrEdit";
	private static final String VIEWS_MENU_SHOW_FORM = "sys/menu/show";
	private static final String VIEWS_MENU_LIST_FORM = "sys/menu/menuList";
	@Autowired
	private IMenuService menuService;

	@GetMapping("/create")
	public ModelAndView initCreationForm(Map<String, Object> model) {
		Menu menu = new Menu();
		model.put("menu", menu);
		return new ModelAndView(VIEWS_MENU_CREATE_OR_EDIT_FORM);
	}

	@PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
	public RESTResponse processCreationForm(@Valid Menu menu, BindingResult result) {
		if (result.hasErrors()) {
			return RESTResponse.createFailure("Incorrect", null);
		} else {
			menuService.store(menu);
			return RESTResponse.createSuccess("Successfully Created", null);
		}
	}

	@PostMapping("/{id}/edit")
	public ModelAndView processUpdateOwnerForm(@Valid Menu menu, BindingResult result, @PathVariable("id") UUID id) {
		if (result.hasErrors()) {
			return new ModelAndView(VIEWS_MENU_CREATE_OR_EDIT_FORM);
		} else {
			menu.setId(id);
			menuService.store(menu);
			return new ModelAndView("redirect:/menu/v1/list");
		}
	}

	@GetMapping("/list")
	public ModelAndView findAll(Menu menu, BindingResult result, Map<String, Object> model) {
		List<Menu> enitityList = menuService.getAll();
		model.put("enitityList", enitityList);
		return new ModelAndView(VIEWS_MENU_LIST_FORM);
	}

	/**
	 * Custom handler for displaying an menu.
	 *
	 * @param Id
	 *            the ID of the owner to display
	 * @return a ModelMap with the model attributes for the view
	 */
	@GetMapping("/show/{Id}")
	public ModelAndView show(@PathVariable("Id") UUID id) {
		Menu entity = menuService.getById(id);
		ModelAndView mav = new ModelAndView(VIEWS_MENU_SHOW_FORM);
		mav.addObject(entity);
		return mav;
	}
	
}

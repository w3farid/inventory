package com.inventory.controller.sys;

import com.inventory.controller.ICommonController;
import com.inventory.model.sys.Menu;

public interface IMenuController extends ICommonController<Menu> {

}

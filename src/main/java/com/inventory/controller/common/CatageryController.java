package com.inventory.controller.common;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.inventory.model.User;

@RestController
@RequestMapping("/category")
public class CatageryController {
	
	@RequestMapping(value="/addCatagory", method = RequestMethod.GET)
	public ModelAndView addCatagory(){
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		modelAndView.addObject("user", user);
		modelAndView.setViewName("category/createCatagory");
		return modelAndView;
	}

	

}

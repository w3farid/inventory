package com.inventory.controller.common;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HomeController {
	
	@RequestMapping("/dashboard")
	public ModelAndView index() {
		return new ModelAndView("home");
	}
	
	@RequestMapping("/test")
	public String test() {
		return "home";
	}
	
}

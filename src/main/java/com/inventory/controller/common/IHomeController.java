package com.inventory.controller.common;

import com.inventory.controller.ICommonController;
import com.inventory.model.sys.Menu;

public interface IHomeController extends ICommonController<Menu> {

}

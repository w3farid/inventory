package com.inventory.repository;

import com.inventory.model.sys.Menu;

public interface IMenuRepository extends ICommonRepository<Menu> {

}

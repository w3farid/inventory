package com.inventory.repository;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.inventory.configuration.exception.ResourceNotFoundException;
import com.inventory.model.sys.Menu;
@Repository("menuRepository")
public class MenuRepository implements IMenuRepository {
	@Autowired
	private SessionFactory session;
	
	@SuppressWarnings("unchecked")
	@Transactional
	@Override
	public List<Menu> getAllDoc() {
		return (List<Menu>) session.getCurrentSession().createQuery("FROM Menu").list();
	}
	
	@Transactional
	@Override
	public Menu getDocById(UUID id) {
		return session.getCurrentSession().get(Menu.class, id);
	}

	@Override
	public List<Menu> getDocs(Map<String, String> params) {
		// TODO Auto-generated method stub
		return null;
	}
	@Transactional
	@Override
	public UUID insertDoc(Menu entity) {
		try {
			session.getCurrentSession().save(entity);
			session.getCurrentSession().flush();
		} catch (Exception e) {
			throw new ResourceNotFoundException((long) 1001, e.getMessage());
		}
		return entity.getId();
	}
	@Transactional
	@Override
	public UUID updateDoc(Menu entity) {
		session.getCurrentSession().update(entity);
		session.getCurrentSession().flush();
		return entity.getId();
	}

	@Override
	public UUID deleteDoc(UUID id) {
		// TODO Auto-generated method stub
		return null;
	}

	

}

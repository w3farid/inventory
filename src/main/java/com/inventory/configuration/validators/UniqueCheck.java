package com.inventory.configuration.validators;
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;
 
@Documented
@Constraint(validatedBy = UniqueCheckValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueCheck {
	String value() default "{uniquCheckValue}";
	String tableName() default "{tableName}";
	String fieldName() default "{fieldName}";
    String message() default "{Must be unique}";
 
    Class<?>[] groups() default {};
 
    Class<? extends Payload>[] payload() default {};
}
package com.inventory.configuration.validators;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueCheckValidator implements ConstraintValidator<UniqueCheck, String> {
    
	private String tableName;
	private String fieldname;
	
    @Override
	public void initialize(UniqueCheck uniqueCheck) {
    	this.tableName = uniqueCheck.tableName();
    	this.fieldname = uniqueCheck.fieldName();
		
	}

    public boolean isValid(String value, ConstraintValidatorContext context) {
    	
        if ("mydevgeek".equalsIgnoreCase(value)) {
            return false;
        }
        return true;
    }

}
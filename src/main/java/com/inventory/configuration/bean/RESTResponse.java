package com.inventory.configuration.bean;

import java.util.UUID;

public class RESTResponse {
	private String outcome;
	private String message;
	private UUID id;
	private String code;
	private String mode;
	private Object data;
	private String[] error;
	
	public static RESTResponse createSuccess(String msg, UUID id, String mode, Object data){
		RESTResponse RR = new RESTResponse();
		RR.outcome = "success";
		RR.message = msg;
		RR.id = id;
		RR.mode = mode;
		RR.data = data;
		return RR;
	}
	
	public static RESTResponse createSuccess(String msg, UUID id, Object data){
		RESTResponse RR = new RESTResponse();
		RR.outcome = "success";
		RR.message = msg;
		RR.id = id;
		RR.data = data;
		return RR;
	}
	
	public static RESTResponse createSuccess(String msg, Object data){
		RESTResponse RR = new RESTResponse();
		RR.outcome = "success";
		RR.message = msg;
		RR.data = data;
		return RR;
	}
	
	public static RESTResponse createFailure(String msg,String[] error){
		RESTResponse RR = new RESTResponse();
		RR.outcome = "error";
		RR.message = msg;
		RR.error = error;
		return RR;
	}
	


	public String[] getError() {
		return error;
	}

	public void setError(String[] error) {
		this.error = error;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public RESTResponse(String outcome, String message, UUID id, String code, String mode, Object data) {
		super();
		this.outcome = outcome;
		this.message = message;
		this.id = id;
		this.code = code;
		this.mode = mode;
		this.data = data;
	}

	
	public RESTResponse(String outcome, String message, UUID id, String code, String mode, Object data, String[] error) {
		super();
		this.outcome = outcome;
		this.message = message;
		this.id = id;
		this.code = code;
		this.mode = mode;
		this.data = data;
		this.error = error;
	}

	public RESTResponse() {
		super();
	}

	public String getOutcome() {
		return outcome;
	}
	public String getMessage() {
		return message;
	}
	public UUID getId() {
		return id;
	}
	public String getCode() {
		return code;
	}
	public String getMode() {
		return mode;
	}
	public void setOutcome(String outcome) {
		this.outcome = outcome;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}

/*	@Override
	public boolean equals(Object obj) {
		
		return this.equals(obj);
	}*/

	
	
	

}

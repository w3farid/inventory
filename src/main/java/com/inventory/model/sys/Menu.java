package com.inventory.model.sys;

import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "sys_menus",uniqueConstraints = {@UniqueConstraint(columnNames = {"menu_code"})})
public class Menu {
	@Id
	@Column(name = "id")
	@org.hibernate.annotations.Type(type="org.hibernate.type.PostgresUUIDType")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private UUID id;
	
	@Column(name = "menu_seq")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int menuSeq;
	
	@Column(name = "menu_code", unique=true)
	@Length(min = 2,max = 7, message = "*Menu code must have at least 2 and greater than 7 characters")
	@NotEmpty(message = "*Menu code empty not allwed")
	private String menuCode;
	
	@Column(name = "menu_short_name")
	@NotEmpty(message = "*Menu short name empty not allowed")
	private String menuShortName;
	
	@Column(name = "menu_full_name")
	private String menuFullName;
	
	@Column(name = "menu_icon")
	private String menuIcon;
	
	@Column(name = "menu_logo_path")
	private String menuLogoPath;
	
	@Column(name = "menu_description")
	private String menuDescription;
	
	@Column(name = "menu_num_code")
	private String menuNumCode;
	
	@Column(name = "created_by_code")
	private String createdByCode;
	
	@Column(name = "created_by_name")
	private String createdByName;
	
	@Column(name = "created_by_username")
	private String createdByUsername;
	
	@Column(name = "created_by_email")
	private String createdByEmail;
	
	@Column(name = "created_at")
	private Timestamp createdAt;
	
	@Column(name = "updated_by_code")
	private String updated_by_code;
	
	@Column(name = "updated_by_username")
	private String updated_by_username;
	
	@Column(name = "updated_by_email")
	private String updated_by_email;
	
	@Column(name = "updated_at")
	private Timestamp updated_at;
	
	@OneToMany
	@JoinColumn(name="menu_id")
	private Set<Privilege> priv;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public int getMenuSeq() {
		return menuSeq;
	}

	public void setMenuSeq(int menuSeq) {
		this.menuSeq = menuSeq;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public String getMenuShortName() {
		return menuShortName;
	}

	public void setMenuShortName(String menuShortName) {
		this.menuShortName = menuShortName;
	}

	public String getMenuFullName() {
		return menuFullName;
	}

	public void setMenuFullName(String menuFullName) {
		this.menuFullName = menuFullName;
	}

	public String getMenuIcon() {
		return menuIcon;
	}

	public void setMenuIcon(String menuIcon) {
		this.menuIcon = menuIcon;
	}

	public String getMenuLogoPath() {
		return menuLogoPath;
	}

	public void setMenuLogoPath(String menuLogoPath) {
		this.menuLogoPath = menuLogoPath;
	}

	public String getMenuDescription() {
		return menuDescription;
	}

	public void setMenuDescription(String menuDescription) {
		this.menuDescription = menuDescription;
	}

	public String getMenuNumCode() {
		return menuNumCode;
	}

	public void setMenuNumCode(String menuNumCode) {
		this.menuNumCode = menuNumCode;
	}

	public String getCreatedByCode() {
		return createdByCode;
	}

	public void setCreatedByCode(String createdByCode) {
		this.createdByCode = createdByCode;
	}

	public String getCreatedByName() {
		return createdByName;
	}

	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}

	public String getCreatedByUsername() {
		return createdByUsername;
	}

	public void setCreatedByUsername(String createdByUsername) {
		this.createdByUsername = createdByUsername;
	}

	public String getCreatedByEmail() {
		return createdByEmail;
	}

	public void setCreatedByEmail(String createdByEmail) {
		this.createdByEmail = createdByEmail;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdated_by_code() {
		return updated_by_code;
	}

	public void setUpdated_by_code(String updated_by_code) {
		this.updated_by_code = updated_by_code;
	}

	public String getUpdated_by_username() {
		return updated_by_username;
	}

	public void setUpdated_by_username(String updated_by_username) {
		this.updated_by_username = updated_by_username;
	}

	public String getUpdated_by_email() {
		return updated_by_email;
	}

	public void setUpdated_by_email(String updated_by_email) {
		this.updated_by_email = updated_by_email;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}

	public Set<Privilege> getPriv() {
		return priv;
	}

	public void setPriv(Set<Privilege> priv) {
		this.priv = priv;
	}
	
	
}

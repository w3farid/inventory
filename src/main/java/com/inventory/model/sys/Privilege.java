package com.inventory.model.sys;

import java.sql.Timestamp;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "sys_privs")
public class Privilege {
	@Id
	@Column(name = "id")
	@org.hibernate.annotations.Type(type = "org.hibernate.type.PostgresUUIDType")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;

	@Column(name = "priv_code")
	@NotNull(message = "Privilege code empty not allowed")
	private String privCode;

	@Column(name = "priv_name", unique = true)
	@Length(min = 2, max = 7, message = "*Menu code must have at least 2 and greater than 7 characters")
	@NotEmpty(message = "*Menu code empty not allwed")
	private String privName;

	@Column(name = "priv_seq")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int privSeq;

	@Column(name = "page_link")
	private String pageLink;

	@Column(name = "tcode", unique = true)
	private String tcode;

	@Column(name = "description")
	private String description;

	@Column(name = "super_admin")
	private boolean superAdmin;

	@Column(name = "auto_granted")
	private boolean autoGranted;

	@Column(name = "priv_enable")
	private boolean privEnable;

	@Column(name = "menu_id")
	private UUID menuId;
	
	@ManyToOne
	@JoinColumn(name="menu_id", insertable=false, updatable=false)
	private Menu menu;
	
	@Column(name = "created_by_code")
	private String createdByCode;

	@Column(name = "created_by_name")
	private String createdByName;

	@Column(name = "created_by_username")
	private String createdByUsername;

	@Column(name = "created_by_email")
	private String createdByEmail;

	@Column(name = "created_at")
	private Timestamp createdAt;

	@Column(name = "updated_by_code")
	private String updated_by_code;

	@Column(name = "updated_by_username")
	private String updated_by_username;

	@Column(name = "updated_by_email")
	private String updated_by_email;

	@Column(name = "updated_at")
	private Timestamp updated_at;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getPrivCode() {
		return privCode;
	}

	public void setPrivCode(String privCode) {
		this.privCode = privCode;
	}

	public String getPrivName() {
		return privName;
	}

	public void setPrivName(String privName) {
		this.privName = privName;
	}

	public int getPrivSeq() {
		return privSeq;
	}

	public void setPrivSeq(int privSeq) {
		this.privSeq = privSeq;
	}

	public String getPageLink() {
		return pageLink;
	}

	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}

	public String getTcode() {
		return tcode;
	}

	public void setTcode(String tcode) {
		this.tcode = tcode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isSuperAdmin() {
		return superAdmin;
	}

	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}

	public boolean isAutoGranted() {
		return autoGranted;
	}

	public void setAutoGranted(boolean autoGranted) {
		this.autoGranted = autoGranted;
	}

	public boolean isPrivEnable() {
		return privEnable;
	}

	public void setPrivEnable(boolean privEnable) {
		this.privEnable = privEnable;
	}

	public UUID getMenuId() {
		return menuId;
	}

	public void setMenuId(UUID menuId) {
		this.menuId = menuId;
	}

	

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public String getCreatedByCode() {
		return createdByCode;
	}

	public void setCreatedByCode(String createdByCode) {
		this.createdByCode = createdByCode;
	}

	public String getCreatedByName() {
		return createdByName;
	}

	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}

	public String getCreatedByUsername() {
		return createdByUsername;
	}

	public void setCreatedByUsername(String createdByUsername) {
		this.createdByUsername = createdByUsername;
	}

	public String getCreatedByEmail() {
		return createdByEmail;
	}

	public void setCreatedByEmail(String createdByEmail) {
		this.createdByEmail = createdByEmail;
	}

	public Timestamp getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdated_by_code() {
		return updated_by_code;
	}

	public void setUpdated_by_code(String updated_by_code) {
		this.updated_by_code = updated_by_code;
	}

	public String getUpdated_by_username() {
		return updated_by_username;
	}

	public void setUpdated_by_username(String updated_by_username) {
		this.updated_by_username = updated_by_username;
	}

	public String getUpdated_by_email() {
		return updated_by_email;
	}

	public void setUpdated_by_email(String updated_by_email) {
		this.updated_by_email = updated_by_email;
	}

	public Timestamp getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}

}

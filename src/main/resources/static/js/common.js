mainPathUrl = "";

function LoadMainContent(url){
    var navigateAway = function(){
       mainContentUrl = url;
       $.ajax({
        url: url,
        success:function( response, status, xhr ) { alert();
        	if(response.indexOf("<!DOCTYPE html>") != -1){
        		window.location = url;
        	}
        	else{	        		
   /*
 	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        						AUTO SCROLL STOP ON PAGE LOAD
   	+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    */	        				        		
    //$('.main-content').html(response);
		$('.main-content').hide().html(response).fadeIn(500, function(){
			ScrollToTop();
		});
   }
   
}
});
   };

}

function GetParameterByName(url, name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    results = regex.exec(url);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function postMethodSubmition(idClass){
		  var $form = $('#'+idClass);
		  $form.on('submit', function(e) {
		    e.preventDefault();
		    $.ajax({
		      url: $form.attr('action'),
		      type: 'post',
		      data: $form.serialize(),
		      success: function(response) {
		        // if the response contains any errors, replace the form
		    	  console.log(response);
		        if ($(response).find('.has-error').length) {
		          $form.replaceWith(response);
		        } else {
		          // in this case we can actually replace the form
		          // with the response as well, unless we want to 
		          // show the success message a different way
		        	
		        }
		      }
		  });
		});
}






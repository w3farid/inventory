------------------------------------
---Farid 26-11-2017
------------------------------------
INSERT INTO public.sys_roles(role)
    VALUES ('ADMIN');
 
------------------------------------
---Farid 26-11-2017
------------------------------------

CREATE TABLE sys_menus
(
  id 						uuid 							NOT NULL DEFAULT uuid_generate_v4(),
  menu_seq 					integer 						NOT NULL,
  menu_code 				character varying,
  menu_short_name 			character varying,
  menu_full_name 			character varying,
  menu_icon 				character varying,
  menu_logo_path 			character varying,
  menu_description 			character varying,
  created_by_code 			character varying,
  created_by_name 			character varying,
  created_by_username 		character varying,
  created_by_email 			character varying,
  created_at 				timestamp without time zone,
  updated_by_code 			character varying,
  updated_by_name 			character varying,
  updated_by_username 		character varying,
  updated_by_email 			character varying,
  updated_at 				timestamp without time zone,
  menu_num_code 			character varying,
  CONSTRAINT 				pk_sys_menus 					PRIMARY KEY (id)
);

CREATE TABLE sys_privs
(
  id 						uuid 							NOT NULL DEFAULT uuid_generate_v4(),
  priv_code 				character varying 				NOT NULL,
  priv_name 				character varying,
  priv_seq 					integer,
  page_link 				character varying,
  tcode 					character varying,
  description 				character varying,
  super_admin 				boolean 						DEFAULT false,
  auto_granted 				boolean 						DEFAULT false,
  created_by_code 			character varying,
  created_by_name 			character varying,
  created_by_username 		character varying,
  created_by_email 			character varying,
  created_by_company_code 	character varying,
  created_by_company_name 	character varying,
  created_at 				timestamp without time zone,
  updated_by_code 			character varying,
  updated_by_name 			character varying,
  updated_by_username 		character varying,
  updated_by_email 			character varying,
  updated_by_company_code 	character varying,
  updated_by_company_name 	character varying,
  updated_at 				timestamp without time zone,
  priv_enable 				boolean 						DEFAULT true,
  menu_id 					uuid 							NOT NULL 			DEFAULT uuid_generate_v4(),
  CONSTRAINT 				pk_sys_privs 					PRIMARY KEY 		(id),
  CONSTRAINT 				fk_sys_menus 					FOREIGN KEY (menu_id) REFERENCES sys_menus (id) MATCH SIMPLE  
   ON UPDATE NO ACTION ON DELETE NO ACTION
);

ALTER TABLE sys_menus ADD CONSTRAINT cons_menu_code UNIQUE (menu_code);
ALTER TABLE public.sys_users ADD COLUMN username character varying;



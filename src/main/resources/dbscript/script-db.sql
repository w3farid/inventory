------------------------------------
---create Database
------------------------------------
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
DROP DATABASE IF EXISTS inventory;
CREATE DATABASE inventory;

------------------------------------
---create sys_roles table
------------------------------------
DROP TABLE IF EXISTS sys_roles;

CREATE TABLE sys_roles
(
  id 			uuid 						NOT NULL DEFAULT 		uuid_generate_v4(),  
  role 			character varying,
  CONSTRAINT 	pk_sys_roles 				PRIMARY KEY 			(id)
);

------------------------------------
---create sys_roles table
------------------------------------

DROP TABLE IF EXISTS sys_roles;

CREATE TABLE sys_users
(
  id 			uuid 						NOT NULL DEFAULT 		uuid_generate_v4(),
  active 		integer,
  email 		character varying,
  last_name 	character varying,
  name 			character varying,
  password 		character varying,
  CONSTRAINT 	pk_sys_users 				PRIMARY KEY (id)
);


------------------------------------
---create sys_user_roles table
------------------------------------

DROP TABLE IF EXISTS sys_user_roles;

CREATE TABLE sys_user_roles
(
  user_id 		uuid 						NOT NULL DEFAULT 		uuid_generate_v4(),
  role_id 		uuid 						NOT NULL DEFAULT 		uuid_generate_v4(),
  CONSTRAINT 	pk_sys_user_roles 			PRIMARY KEY (user_id, role_id),
  CONSTRAINT 	fk_sys_roles 				FOREIGN KEY (role_id)      					REFERENCES sys_roles (id) MATCH SIMPLE      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT 	fk_sys_users 				FOREIGN KEY (user_id)      					REFERENCES sys_users (id) MATCH SIMPLE      ON UPDATE NO ACTION ON DELETE NO ACTION
);




